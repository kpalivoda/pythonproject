## How to execute scripts

1. Click **Clone** on the right top corner in order to download repository.
2. Open project via **PyCharm IDE**.
3. Install all dependencies from *requirements.txt* file.
4. Specify **Python interpreter** in *PyCharm/preferences/project*.
5. Specify **default test runner** in *PyCharm/preferences/tools/python integrated tools*.
6. Before running scripts, replace **executable path** to Chrome driver *(pythonproject/venv/selenium/webdriver/Chrome/chromedriver)*

---

