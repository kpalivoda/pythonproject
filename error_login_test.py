import allure
import time
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait

SIGNIN_BUTTON = "#login > div.auth-form-body.mt-3 > form > div > input.btn.btn-primary.btn-block"

def test_login_error():
    driver = WebDriver(
        executable_path='/Users/karinapolivoda/PycharmProjects/pythonproject/venv/selenium/webdriver/Chrome/chromedriver')
    with allure.step('Open https://github.com/login'):
        driver.get('https://github.com/login')

    # with allure.step('accept cookie'):
    #     iframe = WebDriverWait(driver, 5).until(ec.element_to_be_clickable((By.ID, "sp_message_iframe_364840")))
    #     driver.switch_to.frame(iframe)
    #     cookie_button = WebDriverWait(driver, 5).until(ec.element_to_be_clickable((By.CSS_SELECTOR, "#notice > div:nth-child(6) > div:nth-child(2) > button")))
    #     cookie_button.click()
    #     driver.switch_to.default_content()

    # with allure.step('log in'):
    #     login_button = WebDriverWait(driver, 5).until(
    #         ec.element_to_be_clickable((By.CSS_SELECTOR, LOGIN_BUTTON)))
    #     login_button.click()


    with allure.step('Enter creds'):
        email_input = driver.find_element_by_id('login_field')
        password_input = driver.find_element_by_id('password')
        login_button = driver.find_element_by_css_selector(SIGNIN_BUTTON)
        email_input.send_keys('pixecay496@fleeebay.com')
        password_input.send_keys('Qwerty123')
        login_button.click()

    with allure.step("Log In failed"):
        error_message = WebDriverWait(driver, 5).until(
            ec.visibility_of_element_located((By.XPATH, "//*[contains(., 'Incorrect username or password.')]")))
        assert error_message is not None

        time.sleep(3)
        driver.close()

