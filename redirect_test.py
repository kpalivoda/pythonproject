import allure
import time
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait

def test_redirect():
    driver = WebDriver(
        executable_path='/Users/karinapolivoda/PycharmProjects/pythonproject/venv/selenium/webdriver/Chrome/chromedriver')
    with allure.step('Open https://github.com/login'):
        driver.get('https://github.com/login')

    with allure.step('Click "Create an account"'):
        create_button = WebDriverWait(driver, 5).until(
            ec.element_to_be_clickable((By.CSS_SELECTOR, '#login > p > a')))
        create_button.click()

    with allure.step("Url changed"):
         WebDriverWait(driver, 5).until(
            ec.url_changes('https://github.com/signup'))

    time.sleep(3)
    driver.close()
